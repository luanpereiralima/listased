package ufc.br

object Questao01 {

  trait MetodosHash {
   def criarTabelaDispersao (tamanho : Int)
   def inserirElemento(valor: Integer)
   def funcaoHash(valor : Integer)
   def buscarDeterminadoElemento(value: Integer) : Array[Lista]
   def removerDeterminadoElemento(valor: Integer)
   def liberarTabelaDispersao()
  }

  // HASH TABLE
  class HashTable {
      var tamanho:Integer = 0
      var impl : Implementacao = null
      var tabela:Array[Lista] = null
      
      def criarTabelaDispersao(tam : Int) = {
        impl = new Implementacao
        tamanho = tam/2
        tabela = new Array[Lista](tamanho)
      }
      
      def inserirElemento(valor : Integer)={
        if( tabela(funcaoHash(valor)) == null ){
          var l : Lista = impl.criarLista()
          impl.inserirElementoNoInicio(l, valor)
          tabela(funcaoHash(valor)) = l
        }else{
          impl.inserirElementoNoInicio(tabela(funcaoHash(valor)), valor)
        }
      } 
      
      def funcaoHash(valor : Integer):Integer ={
        valor%tamanho
      }
      
      def buscarDeterminadoElemento(valor : Integer): Lista = {
        impl.buscarDeterminadoElementoDaLista(tabela(funcaoHash(valor)), valor)
    	}
  
      def removerDeterminadoElemento(valor:Integer) = {
        impl.removerDeterminadoElementoDaLista(tabela(funcaoHash(valor)), valor)
  	  }
           
      def liberarTabelaDispersao()={
        tabela = null
        impl = null
      }
  }
  
  def main (args : Array[String]) : Unit  = {
      var hash:HashTable = new HashTable
        
        hash.criarTabelaDispersao(22)
        println("Criada uma tabela hash de tamanho 22")
        
        hash.inserirElemento(3)
        hash.inserirElemento(22)
        hash.inserirElemento(4)
        hash.inserirElemento(99)
        println("Adicionado os valores 3, 4, 22 3 99")
        
        var pos = hash.buscarDeterminadoElemento(3)
        println("O valor 3 esta no elemento: " + pos)
        pos = hash.buscarDeterminadoElemento(1)
        println("O valor 1 esta no elemeto: " + pos)
        pos = hash.buscarDeterminadoElemento(99)
        println("O valor 99 esta no elemeto: " + pos)
        
        hash.removerDeterminadoElemento(3)
        println("O valor 3 foi removido")
        
        hash.liberarTabelaDispersao()
        println("tabela liberada")
  }
  
  //LISTA IMPLEMENTADA
  
  trait MetodosListaEncadeada {
    def criarLista() : Lista
    def inserirElementoNoInicio(lista : Lista, valor : Integer)
    def imprimirValoresDaLista(lista : Lista)
    def imprimirValoresDaListaRecursao(lista : Lista, primeiro : Boolean)
    def imprimirValoresDaListaOrdemReversa(lista : Lista, primeiro : Boolean) : Unit
    def verificarSeListaVazia(lista : Lista): Integer
    def buscarDeterminadoElementoDaLista(lista : Lista, elemento : Integer): Lista
    def removerDeterminadoElementoDaLista(lista: Lista, valor : Integer)
    def removerDeterminadoElementoDaListaRecursao(lista: Lista, valor : Integer, primeiro : Boolean, anterior : Lista)
    def liberarLista(lista : Lista)
  }
  
  class Lista(valorParam : Integer = null){
    var valor : Integer = valorParam
    var proximo : Lista = null
    override def toString : String = {
      " Valor: "+valor+ "\n"+(if (proximo != null) proximo else "")
    }
  }
  
  class Implementacao extends MetodosListaEncadeada{

    override def criarLista() : Lista = {
      new Lista
    }
    
    override def inserirElementoNoInicio(lista : Lista, valor: Integer) : Unit= {      
      var listaTemp = lista
      
      if(listaTemp == null){
        println("A Lista não está iniciada.")
      }else{
        
        if(listaTemp.valor == null){
          listaTemp.valor = valor
          return
        }else{
          var valorAnterior = listaTemp.valor
          listaTemp.valor = valor
          var novoNo = new Lista(valorAnterior)
          var proximoAnterior = listaTemp.proximo
          listaTemp.proximo = novoNo
          novoNo.proximo = proximoAnterior
        }
      }
    }
    
    override def imprimirValoresDaLista(lista : Lista) = {
      var listaTemp = lista
      
      if(listaTemp == null || listaTemp.valor == null){
        println("A lista está vazia");
      }else{
        do{
          println(listaTemp.valor)
          listaTemp = listaTemp.proximo
        }while(listaTemp!=null)
      }
    }
    override def imprimirValoresDaListaRecursao(lista : Lista, primeiro : Boolean = true) : Unit = {
      var listaTemp = lista
      if((listaTemp == null  || listaTemp.valor == null) && primeiro){
        println("A lista está vazia")
        return
      }else if(listaTemp != null){
        println(listaTemp.valor)
        listaTemp = listaTemp.proximo
        imprimirValoresDaListaRecursao(listaTemp, false)
      }
    }
    
    override def imprimirValoresDaListaOrdemReversa(lista : Lista, primeiro : Boolean = true) : Unit = {
      if((lista == null || lista.valor == null) && primeiro){
         println("A lista está vazia")
         return
      }else if(lista != null){
          this.imprimirValoresDaListaOrdemReversa(lista.proximo, false)
          println(lista.valor)
      }
    }
    
    override def verificarSeListaVazia(lista : Lista): Integer = {
      if (lista == null || lista.valor == null) 1 else 0
    }
    
    override def buscarDeterminadoElementoDaLista(lista : Lista, elemento : Integer): Lista = {
      if(lista == null || lista.valor == null){
        println("A lista está vazia")
        null
      }else{
        var listaTemp = lista;
        
        do{
          if(listaTemp.valor == elemento)
            return listaTemp

          listaTemp = listaTemp.proximo   
        }while(listaTemp != null)
        null
      }
    }
    
    override def removerDeterminadoElementoDaLista(lista: Lista, valor : Integer) : Unit = {
       if(lista == null || lista.valor == null){
         println("A lista está vazia")
       }else{       
       
         var tempList = lista
         var anterior : Lista = null
         
         do{
           if(tempList.valor == valor){
           
             if(anterior!=null){
               anterior.proximo = tempList.proximo
               tempList = anterior
             }else{
               if(tempList.proximo!=null){
                 tempList.valor = tempList.proximo.valor
                 tempList.proximo = tempList.proximo.proximo
               }else{
                 tempList.valor = null
               }
             }
             return
             
           }else{
             anterior = tempList
             tempList = tempList.proximo
           }
         }while(tempList != null)
       }
    }
    
    override def removerDeterminadoElementoDaListaRecursao(lista: Lista, valor : Integer, primeiro : Boolean = true, anterior : Lista = null)  = {
      if(lista != null){
        var temp = lista
        if(temp.valor == valor){
          
          if(primeiro){
            if(temp.proximo!=null){
              temp.valor = temp.proximo.valor
              temp.proximo = temp.proximo.proximo
            }else{
              temp.valor = null
            }
          }else{
            if(anterior!=null){
              anterior.proximo = temp.proximo
            }else{
              temp = temp.proximo
            }
          }
        }else{
          removerDeterminadoElementoDaListaRecursao(temp.proximo, valor, false, temp)
        }
      }
    }
    
    override def liberarLista(lista : Lista) = {
       if(lista == null || lista.valor == null){
         println("A lista está vazia")
       }else{
         var proximo = lista
         var apagar = proximo
         
         do{
          proximo.valor = null
          apagar = proximo
          proximo = proximo.proximo
          apagar = null
         }while(proximo!=null)
       }
    }
  }
  
}