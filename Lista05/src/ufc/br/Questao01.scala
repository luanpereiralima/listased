package ufc.br

import scala.collection.mutable.ArrayBuffer

object Questao01 {

   trait MetodosHeap{
      def cria(max : Int) : Heap
      def insere(heap : Heap, valor : Integer)
      def remove(heap : Heap)
      def alterar(hp : Heap, posicao : Int, valor : Integer)
      def print(heap : Heap)
      def buscar(heap : Heap, valor : Integer) : Integer
      def liberar(heap : Heap)
   }
   
   class Heap{
    var tamanho : Int = 0
    var pos : Int = 0
    var prioridades : ArrayBuffer[Integer] = null
   }
   
   class ImplHeap extends MetodosHeap{
      override def cria(tamanho : Int) : Heap = {
        var heap : Heap = new Heap
        heap.tamanho = tamanho
        heap.pos = 0
        heap.prioridades = new ArrayBuffer[Integer](tamanho)
        heap
      }
      override def insere(heap : Heap, valor : Integer){
        if(heap.pos <= heap.tamanho-1){
          heap.prioridades += valor
          corrige_acima(heap, heap.pos);
          heap.pos += 1
        }else{
          println("heap cheia")
        }
      }
      
      def troca( a : Int,  b : Int, hp : Heap) {
        var i : Integer = hp.prioridades(a);
        hp.prioridades(a) = hp.prioridades(b);
        hp.prioridades(b) = i;
      }
      
      def corrige_acima(hp : Heap, pos : Int){
        var posAux = pos
        
        while(posAux > 0){
          var pai : Int = (posAux - 1)/2
          if(hp.prioridades(pai) < hp.prioridades(posAux)){
            troca(posAux, pai, hp)
          }else{
            return
          }
          posAux = pai
        }
      }
      
      override def remove(heap : Heap){
         if(heap.pos > 0){
           var topo : Integer = heap.prioridades(0)
           heap.prioridades(0) =  heap.prioridades(heap.pos-1)
           heap.prioridades.remove(heap.pos-1)
           heap.pos -= 1
           corrige_abaixo(heap)
         }else{
           println("Heap vazia")
         }
      }
      
      def corrige_abaixo (heap : Heap, posicao : Int = 0){
        
        var pai : Int = posicao
        
        while((2*pai+1) < heap.pos){
          var filho_esq : Int = 2*pai+1
          var filho_dir : Int = 2*pai+2
          
          var filho : Int = 0
          
          if(filho_dir >= heap.pos){
            filho_dir = filho_esq
          }
          
          if(heap.prioridades(filho_esq) > heap.prioridades(filho_dir)){
            filho = filho_esq
          }else{
            filho = filho_dir
          }
          
          if(heap.prioridades(pai) < heap.prioridades(filho)){
            troca(pai, filho, heap)
          }else
            return
            
          pai = filho
        }
      }
      
      override def print(hp : Heap) {
        if(hp.prioridades!=null)
          println(hp.prioridades.toList)
        else
          println("Lista não iniciada")
      }
      
      override def alterar(hp : Heap, posicao : Int, valor : Integer) {
        if(valor >= hp.prioridades(posicao)){
          hp.prioridades(posicao) = valor
          corrige_acima(hp, posicao)
        }else{
          hp.prioridades(posicao) = valor
          corrige_abaixo(hp, posicao)
        }
        
      }
      
      def buscar(heap : Heap, valor : Integer) : Integer = {
        for(x <- 0 to (heap.pos-1)){
          if(heap.prioridades(x) == valor)
            return valor
        }
        return null
      }
      
      override def liberar(heap : Heap) {
        heap.prioridades = null
        heap.tamanho = 0
        heap.pos = 0
      }
   }
   
   def main(args : Array[String]) {
     var impl : ImplHeap = new ImplHeap
     var hp : Heap = impl.cria(21)
     
     for(m <- 1 to 20){
       impl.insere(hp, m);
       println(hp.prioridades.toList)
     }
     
     println(hp.prioridades.toList)
     
     println("REMOVENDO")
     
     impl.remove(hp)
     
     impl.print(hp)
     
     impl.remove(hp)
     
     impl.print(hp)
     
     impl.remove(hp)
     
     impl.print(hp)
     
     impl.remove(hp)
     
     impl.print(hp)
     
     impl.remove(hp)
     
     impl.print(hp)
     
     println(impl.buscar(hp, 2))
     
     impl.alterar(hp, 0, 0)
     
     impl.print(hp)
     
     impl.liberar(hp)
     
     impl.print(hp)
     
     
   }
  
}