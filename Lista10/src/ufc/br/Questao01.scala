package ufc.br

object Questao01 {
  def main(args: Array[String]): Unit = {
    /*
     * 1) Seja uma árvore B de ordem d = 3 e altura h = 4. Qual o número máximo de chaves na árvore? E o numero mínimo? Justifique.
     * 
     * 	O número máxima de chaves é definido pela formula: m^(h+1) - 1
     * 	substituindo: 3 ^ (5) - 1 = 242
     * 	Logo, a máximo de chaves é 242
     * 
     * 	O número mínimo de chaves é definida pela formula: k = 2 * ([m/2]^h -1)
     * 	De acordo com as definições abaixo: 
     * • A raiz tem no mínimo 1 chave e 2 filhos.
		 * • Todos os nós internos tem no mínimo [m/2] filhos e ([m/2] -1) chaves.
		 * • Todas as folhas tem no mínimo ([m/2] -1) chaves.
		 * • Logo, o número mínimo de chaves será: 
		 * 
		 * 	Substituindo: 2 * (([3/2]^4) - 1) = 2 * ((2^4) - 1) = 2 * 15 = 30
		 * 
		 * 	Logo, a mínimo de chaves é 30
     * 
     */
  }
}