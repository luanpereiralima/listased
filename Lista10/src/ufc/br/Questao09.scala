package ufc.br

object Questao09 {
  
  class Node (var chave:Array[Int], var parent:Node, var ramo:Array[Node], 
          var ndesc:Int, val MAX:Int, val Min:Int) {
  
  def this(){
    
    this(null, null, null,0,4,2)
    this.chave = new Array[Int](MAX+1)
    this.chave = this.chave.map(_-1) 
    this.parent = null
    this.ramo = new Array[Node](MAX+1)
    this.ndesc = 0
  
  }

  def printarArvore(lim_inf:Int, lim_sup:Int,arv:Node=this){
    if(arv != null ){
        for(i <- 0 until arv.ndesc){
          printarArvore(lim_inf, lim_sup, arv.ramo(i))
          if(arv.chave(i) >  lim_inf && arv.chave(i) < lim_sup)
            print(arv.chave(i) + " ")
        }
        printarArvore(lim_inf, lim_sup, arv.ramo(arv.ndesc))
    }
  }
  
  def inserir(value:Int, arv:Node=this):Node={
    var arvAux:Node=arv

    if(arvAux.ramo(0) == null ){

        if(arvAux.chave(0) == -1 ){
            arvAux.chave(0) = value
            arvAux.ndesc+=1
        }
        else{
            arvAux = inserirLeaf(arvAux,value)
            while(arvAux.ndesc == MAX+1)
                arvAux = dividir(arvAux)
        }
    }

    else{
        arvAux = searchinserir(arvAux, value)

        arvAux = inserirLeaf(arvAux,value)
        
        while(arvAux.ndesc == MAX+1){
          arvAux = dividir(arvAux)
        }
      }
    arvAux
   }
  
  def searchinserir(Arv:Node, value:Int):Node={
    var arvAux:Node = Arv
    if(arvAux.ramo(0) != null){
          for (i <- 0 until arvAux.ndesc){
               if(arvAux.chave(i) > value){
                 return searchinserir(arvAux.ramo(i), value)
               }
               if(i == arvAux.ndesc-1){
                 return searchinserir(arvAux.ramo(ndesc), value)
               }
          }               
    }
    arvAux
  }
  
  def inserirLeaf(Arv:Node,value:Int):Node={
    var arvAux:Node = Arv
    for(i <- 0 until arvAux.ndesc){
        if(arvAux.chave(i) > value){
          for(j <- i until arvAux.ndesc)
            arvAux.chave(j+1) = arvAux.chave(j)
          arvAux.chave(i) = value
          arvAux.ndesc+=1
        }
        else if(i == arvAux.ndesc-1){
          arvAux.chave(arvAux.ndesc) = value
          arvAux.ndesc+=1
        }
    }
    arvAux
  }
  
  
  def dividir(Arv:Node, left:Node = null, right:Node=null, value:Int=0):Node={
    var arvAux:Node = Arv
    
    
    var arvAux2:Node = new Node()
    var arvAux3:Node = new Node()
    
    if(left != null && right != null && value != 0){
      inserirLeaf(arvAux, value)
    }
      
    for(i <- 0 until (arvAux.ndesc/2)){
      arvAux2.chave(i) = arvAux.chave(i)
      arvAux2.ndesc+=1
    }
    
    for(i <-(arvAux.ndesc/2)+1 until arvAux.ndesc){
      arvAux3.chave(arvAux3.ndesc) = arvAux.chave(i)
      arvAux3.ndesc+=1
    }
    
    if(left != null && right != null && value != 0){
      for(i <- 0 to arvAux.MAX/2){
        arvAux2.ramo(i) = arvAux.ramo(i)  
      }
      for(i <- ((arvAux.MAX/2)+1) until arvAux.MAX){
        arvAux3.ramo(i-((arvAux.MAX/2)+1)) = arvAux.ramo(i)  
      }
      arvAux3.ramo(arvAux3.ndesc-1) = left
      arvAux3.ramo(arvAux3.ndesc) = right
      left.parent = arvAux3
      right.parent = arvAux3
    }
       
    if(arvAux.parent == null ){
      
      var arvNewRoot: Node = new Node()
      arvNewRoot.chave(0) = arvAux.chave(ndesc/2)
      arvNewRoot.ndesc+=1
      arvNewRoot.ramo(0) = arvAux2
      arvNewRoot.ramo(1) = arvAux3
      arvAux2.parent = arvNewRoot
      arvAux3.parent = arvNewRoot
      return arvNewRoot
    }

    else{
      
      var nodeParent:Int = 0
      for(i <- 0 to arvAux.parent.ndesc){
        if(arvAux.parent.ramo(i) == arvAux)
          nodeParent = i
      }
      for(i <- arvAux.parent.ndesc to nodeParent + 1 by -1){
        arvAux.parent.ramo(i+1) = arvAux.parent.ramo(i) 
      }
      
      if(nodeParent < MAX){
        arvAux.parent.ramo(nodeParent) = arvAux2
        arvAux.parent.ramo(nodeParent+1) = arvAux3
        arvAux2.parent = arvAux.parent
        arvAux3.parent = arvAux.parent
        return inserirLeaf(arvAux.parent, arvAux.chave(arvAux.ndesc/2))
      }
      else
        return dividir(arvAux.parent, arvAux2, arvAux3, arvAux.chave(arvAux.ndesc/2))
    }
  }
  
}
  
   
  def main(args: Array[String]): Unit = {
        var arvore:Node = new Node()

    arvore = arvore.inserir(1)
    arvore = arvore.inserir(2)
    arvore = arvore.inserir(3)
    arvore = arvore.inserir(4)    
    arvore = arvore.inserir(5)

    arvore.printarArvore(1, 4)
  }
}