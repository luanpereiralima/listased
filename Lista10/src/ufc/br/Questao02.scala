package ufc.br

object Questao02 {
  def main(args: Array[String]): Unit = {
    /*
     * Os nós internos da árvore B+ possuem apenas chaves, os nós-folha possuem chaves + dados que formam uma lista (duplamente) encadeada
     * Diferente da árvore B, que contém os dados em todos os nós.
     * 
     * A arvore B+ trabalha com registros organizados em blocos, coma lista duplamente encadeada, isso garante acesso sequencial do arquivo.
     * 
     */
  }
}