package ufc.br

object Questao06 {
  def main(args: Array[String]): Unit = {
    /* 
     * pelo fator do mod, sempre que ultrapassamos o número que está após o mod, teremos 
     * repetições, logo:
     */

    //A
     // n/5
     //B
     // n/7
     //C
     // n/35
     //D
     // 5n/m
     //E
     // n/m
  }
}