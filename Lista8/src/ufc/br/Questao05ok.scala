package ufc.br

import scala.collection.mutable.ArrayBuffer

object Questao05 {
  
  class HashExt {
    
    var MAX : Int = 8
    var VAZIO = -1
    
    var hash = new Array[Int](MAX)
    
    def iniciarLista(){
      for(x <- 0 to MAX-1){
        hash(x) = (-1) 
      }
    }
    
    def insere( x : Int ) : Int = {
      
      var pos : Int = 0 
      var k : Int = 0
      pos = x
      
      for (k <- 0 to MAX-1){
        
        pos = (pos + k) % MAX
        
        if (hash(pos) == x)
          return pos
         
        if(hash(pos) == VAZIO){
          hash(pos) = x
          return pos
        } 
      }  
        return (-1)
      }
    
    def listar(){
      println(hash.toList);
    }
 }
  
  def main(args: Array[String]){
    var hash : HashExt = new HashExt
    
    hash.iniciarLista()
    
    //A)
    hash.insere(4)
    
    hash.insere(12)
    
    hash.insere(20)
    
    hash.insere(28)
    
    hash.insere(36)
    
    hash.insere(44)
    
    hash.insere(52)
    
    hash.insere(60)
    
    //B)
    //pos = (pos + k) % MAX;
    
    //C)
    //LINEAR
    
    //D)
    // quando o elemento não encontra espaço no vetor para ser adicionado.
    
    //E)
    //quando o valor de MAX > 0
    
    
    hash.listar()
    
  }
  
}