package ufc.br

import scala.collection.mutable.ArrayBuffer

object Questao01 {
  
  class HashExt {
    
    var tamanho : Int = 513
    
    var lista = new Array[String](tamanho)
    
    //FUNÇÃO DE ESPALHAMENTO 
    def hash(v : String) : Int = {
      
      if(v.length > 32){
        println("A String necessita ser menor que 32 caracteres")
        return -1;
      }
      
      var h: Int = v(0);
      var i: Int = 0
      
      while(v.length > i && v(i) != '\0'){
        h = (h * 509 + v(i)) % tamanho
        i+=1
      }
      
      return h
    }
    
    def adicionarNaTabela(palavra : String){
      lista(hash(palavra)) = palavra
    }
    
    def listar(){
      println(lista.toList);
    }
  }
  
  def main(args: Array[String]){
    var hash : HashExt = new HashExt
    
    hash.adicionarNaTabela("Brasil")
    hash.adicionarNaTabela("Espanha")
    hash.adicionarNaTabela("Argentina")
    hash.adicionarNaTabela("EUA")
    
    hash.listar()
  }
  
}