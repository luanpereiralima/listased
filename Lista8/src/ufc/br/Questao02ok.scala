package ufc.br

import scala.collection.mutable.ArrayBuffer

object Questao02 {
  
  def main(args: Array[String]){
    /*
     * Quando ocorre uma colisão, a chave é
      armazenada na primeira posição livre
      após h(x), a posição d, digamos.
      Se agora incluirmos y tal que h(y)=d,
      teremos a fusão das listas
      correspondentes a h(x) e h(y),
      diminuindo a eficiência do esquema.
      O maior problema dessa abordagem é
      que pode haver colisões secundárias,
      isto é colisões em que h(x) ≠ h(y). 
     */
   
     /*
      * Imagine uma função hash = x mod 1
      * com uma inserção na sequencia dos valores 0 e 11
      * o 11 assumiriam a mesma posião que o 0 na tabela, gerando uma colisão
      * fazendo o 11 ir a posição 1.
      * da mesma forma que o 2, que iria para a 1, mas ela já foi ocupada por 11.
      */
  }
  
}