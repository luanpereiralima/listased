package ufc.br

object Questao09 {
  trait MetodosHash{
    def hsh_cria(n : Int )//hash_func(char*), void free_func(void*));
    def hsh_insere(tabela: Hash, info : Int, chave : String) : Int
    def hsh_remove(tabela : Hash, chave : String) : Int
    def hsh_busca(tabela : Hash, chave : String)
    def hsh_libera(tabela : Hash);
    def hsh_percorre(tabela : Hash);
  }
  
  class Hash{
    
  }
}