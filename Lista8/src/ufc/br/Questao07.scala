package ufc.br

import scala.collection.mutable.ArrayBuffer

object Questao04 {
  
  class HashExt {
    
    var tamanho : Int = 12
    
    var lista = new Array[Int](tamanho)
    
    //FUNÇÃO DE ESPALHAMENTO 
    def hash(v : Int) : Int = {
      v % tamanho
    }
    
    def adicionarNaTabela(numero : Int){
      lista(hash(numero)) = numero
    }
    
    def listar(){
      println(lista.toList);
    }
  }
  
  def main(args: Array[String]){
    var hash : HashExt = new HashExt

    //A)
    hash.adicionarNaTabela(4)
    hash.listar()
    hash.adicionarNaTabela(10)
    hash.listar()
    hash.adicionarNaTabela(24)
    hash.listar()
    hash.adicionarNaTabela(36)
    hash.listar()
    hash.adicionarNaTabela(25)
    hash.listar()
    hash.adicionarNaTabela(14)
    hash.listar()
    hash.adicionarNaTabela(23)
    hash.listar()
    hash.adicionarNaTabela(100)
    hash.listar()
    hash.adicionarNaTabela(2)
    hash.listar()
    hash.adicionarNaTabela(3)
    hash.listar()
    hash.adicionarNaTabela(12)
    hash.listar()
    hash.adicionarNaTabela(13)
    
    hash.listar()
  }
  
}