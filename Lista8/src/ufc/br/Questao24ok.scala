package ufc.br

object Questao24 {
  def main(args: Array[String]): Unit = {
      
    /*
     * MAKESET = O(1) -> O(n) operações
		 * UNION = O(1) ->  O(m) operações
		 * FINDSET O(log n) -> O(m log n) operações
		 * 
		 * Logo, temos O(n) + O(m) + O(m log n), que é O(m lg n) . 
     */
  }
}