

package ufc.br

object Questao10 {

  class No{
    var chave: Integer = null 
    var esquerdo: No = null 
    var direito: No = null 
  }
  
  class implementacaoArvore{
    
    //O ELEMENTO SERÁ ENCONTRADO E RETORNADO O NÓ DO MESMO
    def buscaElemento(no: No, chave : Integer) : No = {
      
      //SE O NÓ É NULO, INDICA QUE O ELEMENTO N EXISTE
      if(no == null)
        return null
        
      //ELEMENTO ENCONTRADO  
      if(no.chave == chave)
        return no
      
      //ELEMENTO NÃO ENCONTRADO, MAS VALOR É MAIOR QUE A CHAVE DO MOMENTO, LOGO PODE ESTAR NA 
      //SUB-ÁRVORE A DIREITA
      if(no.chave > chave)
        return buscaElemento(no.direito, chave)
      
      //ELEMENTO NÃO ENCONTRADO, MAS VALOR É MAIOR QUE A CHAVE DO MOMENTO, LOGO PODE ESTAR NA 
      //SUB-ÁRVORE A ESQUERDA
      if(no.chave < chave)
        return buscaElemento(no.esquerdo, chave)
      
      return null 
    }
  }
  
  def main(args: Array[String]): Unit = {
    
  }
}