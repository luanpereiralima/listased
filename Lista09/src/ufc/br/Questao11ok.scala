

package ufc.br

object Questao11 {

  class No{
    var chave: Integer = null 
    var esquerdo: No = null 
    var direito: No = null 
  }
  
  class implementacaoArvore{
    
    def buscaMaiorElemento(no: No, chave : Integer) : Integer = {
      
      if(no == null)
        return null

      if(no.direito!=null)
        return buscaMaiorElemento(no.direito, chave)
      else
        return chave
      
    }
  }
  
  def main(args: Array[String]): Unit = {
    
  }
}