

package ufc.br

object Questao03 {
  def main(args: Array[String]): Unit = {
    
    //Se uma árvore completa é aquela em que se n é um nó com algumas de subárvores vazias,
    //então n se localiza no penúltimo ou no último nível da árvore.
    
    //E
    
    //Uma árvore binária cheia é uma árvore em que se um nó tem alguma
    //sub-árvore vazia então ele está no último nível
    
    //Logo, a propriedade da árvore binária completa sempre será satisfeita
    //pela árvore binária cheia.
    
  }
}