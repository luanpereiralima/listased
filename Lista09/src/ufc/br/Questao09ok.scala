
package ufc.br

import scala.collection.mutable.Queue
import scala.collection.mutable.ArrayBuffer

object Questao09 {

  class No{
    var chave: Integer = null 
    var esquerdo: No = null 
    var direito: No = null 
  }
  
  class ImplementacaoArvore{
    
     def imprimir(no : No) {
        if (no == null) {
          println("Não pode ser nulo!") 
          return 
        }
        
        var fila = new Queue[No]() 
        
        fila+=(no) 
        
        var tamanhoArvore : Int = this.tamanhoArvore(no)
        
        var filainv : ArrayBuffer[Integer] = new ArrayBuffer[Integer](tamanhoArvore) 
        
        while (!fila.isEmpty) {
            var atual: No = fila.dequeue 
            filainv+=atual.chave
            if (atual.esquerdo != null) 
              fila+=(atual.esquerdo) 
            if (atual.direito != null) 
              fila+=(atual.direito) 
        }
        
        for (i <- filainv.length-1 to (0,-1))
            println(filainv(i))           
    }
     
    def tamanhoArvore(no : No):Int = {
       if(no == null)return 0;
       
       return 1 + tamanhoArvore(no.direito) + tamanhoArvore(no.esquerdo);
    }
     
  }
  
  /*	Exemplo
   *            raiz
   *        no1      no2
   *      no3 no4  no4 no5
   */
  
  def main(args: Array[String]): Unit = {
    
     var impl : ImplementacaoArvore = new ImplementacaoArvore 
    
     var raiz = new No 
     raiz.chave=0 
     
     var no1 = new No 
     no1.chave=1 
     
     var no2 = new No 
     no2.chave=2 
     
     var no3 = new No 
     no3.chave=3 
     
     var no4 = new No 
     no4.chave=4 
     
     var no5 = new No
     no5.chave=5
     
     var no6 = new No 
     no6.chave=6 
     
     raiz.esquerdo = no1 
     raiz.direito = no2 
     no1.esquerdo = no3 
     no1.direito = no4 
     no2.esquerdo = no5
     no2.direito = no6
     
     impl.imprimir(raiz)
     
  }
}