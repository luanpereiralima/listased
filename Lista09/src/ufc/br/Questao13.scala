
package ufc.br

import scala.collection.mutable.Queue
import scala.collection.mutable.ArrayBuffer

object Questao13 {

  class No{
    var chave: Integer = null 
    var esquerdo: No = null 
    var direito: No = null 
    var pai : No = null
  }
  
  class ImplementacaoArvore{
    
    ///antecessor (o nó mais à direita da subárvore esquerda)
    
    def antecessor(no : No) : No = {
      if(no.esquerdo != null){
         return buscaMaisADireita(no.esquerdo)
      }
      
      return null;
    }
    
    def buscaMaisADireita(no: No) : No = {
      
      if(no == null)
        return null

      if(no.direito!=null)
        return buscaMaisADireita(no.direito)
      else
        return no
      
    }
     
  }
  
  /*	Exemplo
   *            raiz
   *        no1      no2
   *      no3 no4  no4 no5
   */
  
  def main(args: Array[String]): Unit = {
    
     var impl : ImplementacaoArvore = new ImplementacaoArvore 
    
     var raiz = new No 
     raiz.chave=0 
     
     var no1 = new No 
     no1.chave=1 
     
     var no2 = new No 
     no2.chave=2 
     
     var no3 = new No 
     no3.chave=3 
     
     var no4 = new No 
     no4.chave=4 
     
     var no5 = new No
     no5.chave=5
     
     var no6 = new No 
     no6.chave=6 
     
     raiz.esquerdo = no1 
     raiz.direito = no2 
     no1.esquerdo = no3 
     no1.direito = no4 
     no2.esquerdo = no5
     no2.direito = no6
     
     println(impl.antecessor(raiz).chave);
     
  }
}