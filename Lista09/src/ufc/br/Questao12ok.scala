

package ufc.br

object Questao12 {

  class No{
    var chave: Integer = null 
    var esquerdo: No = null 
    var direito: No = null 
  }
  
  class ImplementacaoArvore{
    
    def permutacao(no: No) {

      if(no.direito!=null && no.esquerdo!=null){
      	var aux = no.direito
      	no.direito = no.esquerdo
      	no.esquerdo = aux
      	
      	permutacao(no.esquerdo)
        permutacao(no.direito)
      }
      
    }
    
    def imprimir(no : No){
      println(no.chave)
      if(no.esquerdo!=null)
        imprimir(no.esquerdo)
     if(no.direito!=null)
       imprimir(no.direito)
    }
  }
  
  /*	Exemplo
   *            raiz
   *        no1      no2
   *     no3   no4
   */
  
  def main(args: Array[String]): Unit = {
    
     var impl : ImplementacaoArvore = new ImplementacaoArvore 
    
     var raiz = new No 
     raiz.chave=0 
     
     var no1 = new No 
     no1.chave=1 
     
     var no2 = new No 
     no2.chave=2 
     
     var no3 = new No 
     no3.chave=3 
     
     var no4 = new No 
     no4.chave=4 
     
     raiz.esquerdo = no1 
     raiz.direito = no2 
     no1.esquerdo = no3 
     no1.direito = no4 
     
     impl.imprimir(raiz)
     
     impl.permutacao(raiz) 
     
     println("--");
     
     impl.imprimir(raiz)
  }
}