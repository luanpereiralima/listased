
package ufc.br

object Questao01 {
  
  class Conjunto{
    
  	var valores : Array[String] = null
  	var bits: Int = 0
  
  	def criaConjunto(valores : Array[String]) {
    		this.valores = valores
    		bits = 0
  	}
  
  	def insereConjunto(valor : String) {
  		for(i <- 0 to valores.length-1){
  			if(valores(i).equals(valor)){
  				ligarBit(i)
  				return
  			}
  		}
  	}
  
  	def removeConjunto(valor : String) {
  		for(i <- 0 to valores.length-1){
  			if(valores(i).equals(valor)){
  				desligarBit(i)
  				return
  			}
  		}
  	}
  
  	def uniaoConjuntos(conj : Conjunto) : Int = {
  		conj.bits | bits
  	}
  
  	def intersecaoConjuntos(conj : Conjunto) : Int = {
  		conj.bits & bits
  	}
  
    def diferencaConjuntos(conj : Conjunto) : Int = {
  		if(conj.bits < bits)
  			bits - conj.bits
  		else
  			conj.bits - bits
  	}
  
  	def verificaSubconjunto(conj : Conjunto) : Boolean = {
  		if((bits & conj.bits) == conj.bits && (bits | conj.bits) == bits)
  			true
  		false
  	}
  
  	def verificaConjuntosIguais(conj : Conjunto) : Boolean = {
  		(bits == conj.bits)
  	}
  
  	def complementoConjunto() : Int = {
  		~bits
  	}
  
  	def pertenceConjunto(valor : String) : Boolean = {
  		for(i <- 0 to valores.length-1)
  			if(valores(i).equals(valor))
  				estaLigado(i)
  		
  		false
  	}
  
  	def numeroElementosConjunto() : Int = {
  		var valor : Int = bits
  		var bit : Int = 0
  		var contador : Int = 0
  		
  		while(valor != 0){
  			bit = valor%2
  			if(bit == 1)
  				contador += 1
  			valor = valor/2
  		}
  		
  		contador
  	}
  
  	def liberaConjunto() {
  		for(i <- 0 to valores.length-1){
  			valores(i) = null
  		}
  		valores = null
  	}
  	
  	def ligarBit(i : Int) {
  		if(!estaLigado(i))
  			bits += scala.math.pow(2, i).toInt
  	}
  	
  	def desligarBit(i : Int) {
  		if(estaLigado(i))
  			bits -= scala.math.pow(2, i).toInt
  	}
  	
  	def estaLigado(i : Int) : Boolean = {
  		var mask : Int =  1 << i
  		((mask & bits) != 0)
  	}
  	
  	def imprimirConjunto() {
  		for(i <- 0 to valores.length-1){
  			if(estaLigado(i))
  				println(valores(i))
  		}
  		println()
  	}
  }
  
  def main(args : Array[String]) {
		
		println("digite o tamanho Conjunto")
		var n : Int = scala.io.StdIn.readInt()
		
		var valores : Array[String]  = new Array[String](n)	
		for(i <- 0 to n-1)
			valores(i) = ""+i		
		
		var conjunto : Conjunto = new Conjunto()	
		conjunto.criaConjunto(valores)
		
		conjunto.insereConjunto("0")
		conjunto.insereConjunto("1")
		conjunto.insereConjunto("3")
		conjunto.imprimirConjunto()
		
		conjunto.removeConjunto("2")
		conjunto.imprimirConjunto()
		
		var conjunto2 : Conjunto = new Conjunto()	
		conjunto2.criaConjunto(valores)
		conjunto2.insereConjunto("3")
		conjunto2.insereConjunto("5")
		
		conjunto2.bits = (conjunto.uniaoConjuntos(conjunto2))
		
		conjunto2.imprimirConjunto()
		
		var conjunto3 : Conjunto = new Conjunto()	
		conjunto3.criaConjunto(valores)
		conjunto3.insereConjunto("2")
		conjunto3.insereConjunto("1")
		
		conjunto3.bits = (conjunto.intersecaoConjuntos(conjunto3))
		conjunto3.imprimirConjunto()
		
		conjunto2.bits = (conjunto2.diferencaConjuntos(conjunto3))
		conjunto2.imprimirConjunto()
		
		conjunto.imprimirConjunto()
		
		if(conjunto.verificaSubconjunto(conjunto2))
			println("Conjunto 2 é subconjunto de conjunto 1")
		else
			println("Conjunto 2 não é subconjunto de conjunto 1")
		
		if (conjunto.verificaConjuntosIguais(conjunto2))
			println("Conjuntos iguais\n")
		else
			println("Conjuntos diferentes\n")
		
		conjunto3.bits = (conjunto.complementoConjunto())
		conjunto3.imprimirConjunto()
		
		if(conjunto.pertenceConjunto("1"))
			println("Pertence \n")
		else
			println("Não Pertence \n")
		
		println(conjunto.numeroElementosConjunto())
		
		conjunto.liberaConjunto()
		conjunto2.liberaConjunto()
	}
  
}