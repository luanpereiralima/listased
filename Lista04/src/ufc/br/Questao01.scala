package ufc.br

object Questao01 {
  
    
   def main(args : Array[String]){
     
    println("Qtd chaves")
		var n : Int = scala.io.StdIn.readInt()
		println("Tamanho bucket")
		var tamanho : Int = scala.io.StdIn.readInt()
		
		var hash : HashExt = new HashExt()	
		hash.cria(n, tamanho)
		
		for(i <- 1 to n)
			hash.insereHash(i)
		
		hash.imprimeHash()
		
		var nova : Integer = 0
		nova = hash.buscaHash(2)
		println(nova)
	
		hash.removeHash(2)
		hash.removeHash(10)
		hash.imprimeHash()
		
		hash.liberaHash()
   }

   
   class Bucket(tamanho : Int, referencia : Int){
      var valores : Array[Integer] = null
    	var referencia : Int = 0
    	
    	def this(tamanho : Int){
        this(tamanho, 0)
    		valores = new Array[Integer](tamanho)
    	}
    
    	def insereBucket(valor : Int) : Boolean = {
    	  for(i <- 0 to valores.length-1){
    			if (valores(i) == null){
    				valores(i) = valor
    				return true
    			}
    		}
    		return false
    	}
    	
    	def setValor(valor : Int){
    		for(i <- 0 to valores.length-1){
    			if(valores(i) == null){
    				valores(i) = valor
    				return
    			}
    		}
    	}
    	
    	def imprimeBucket() {
    		for(i <- 0 to valores.length-1){
    			if(valores(i) != null)
    				print(valores(i) + " ")
    		}
    	}
    
    	def busca_bucket(valor: Int) : Integer = {
    		for(i <- 0 to valores.length-1){
    			if(valores(i) == valor)
    				return valor
    		}
    		return null
    	}
    
    	def removeBucket(valor : Int) {
    		for(i <- 0 to valores.length-1){
    			if(valores(i) == valor){
    				valores(i) = null
    				return
    			}
    		}
    	}
    
    	def liberaBucket() {
    		for(i <- 0 to valores.length-1){
    			valores(i) = null
    		}
    	}
   }
  
   class HashExt{
	
    var bucket : Array[Bucket] = null
	  var bits_significativos : Int = 0
    var tamanho_bucket : Int = 0
  	
    def cria(n : Int, tamanho : Int) {
  		this.tamanho_bucket = tamanho
  		bits_significativos = 1
  		bucket = new Array[Bucket](1)
  		bucket(0) = new Bucket(tamanho_bucket)
  	}
  
  	def insereHash(valor : Int){
  		var indice : Int = calculaIndice(valor)
  		if(indice%2 == 0){
  		  
  			if(bucket(indice).referencia <  bits_significativos){
  				if(!bucket(indice).insereBucket(valor)){
  					divideBucket(indice)
  					insereHash(valor)
  				}
  			}else if(!bucket(indice).insereBucket(valor)){
					divideTabela()
					insereHash(valor)
				}
  		
  		}else{
  			if(bucket(indice-1).referencia <  bits_significativos){
  				if(!bucket(indice-1).insereBucket(valor)){
  					divideBucket(indice-1)
  					insereHash(valor)
  						
  				}
  			}
  			else if(!bucket(indice).insereBucket(valor)){
  				divideTabela()
  				insereHash(valor)
  			}
  		}
  	}				
  	
  	def divideBucket(indice : Int) {
  		var novo_bucket : Array[Bucket] = new Array[Bucket](bucket.length+1)
  		
  		for(i <- 0 to bucket.length-1){
  			novo_bucket(i) = bucket(i)
  		}
  		
  		novo_bucket(indice+1) = new Bucket(tamanho_bucket)
  		novo_bucket(indice).referencia = bits_significativos
  		novo_bucket(indice+1).referencia = bits_significativos
  		
  		for(i <- 0 to tamanho_bucket-1){
  			var valor : Int = bucket(indice).valores(i)
  			if(calculaIndice(valor) != indice){
  				var novo_indice : Int = calculaIndice(valor)
  				novo_bucket(novo_indice).setValor(valor)
  				novo_bucket(indice).removeBucket(valor)
  			}
  		}
  
  		bucket = novo_bucket
  	}
  
  	def imprimeHash() {
  		for(i <- 0 to bucket.length-1){
  			System.out.print("Bucket " + i + " = ")
  			bucket(i).imprimeBucket()
  			println("")
  		}
  	}
  
  	def buscaHash(valor : Int) : Integer = {
  		var indice : Int = calculaIndice(valor)
  		return bucket(indice).busca_bucket(valor)
  	}
  
  	def removeHash(valor : Int) {
  		var indice : Int = calculaIndice(valor)
  		bucket(indice).removeBucket(valor)
  	}
  
  	def liberaHash() {
  		for(i <- 0 to bucket.length-1){
  			bucket(i).liberaBucket()
  			bucket(i) = null
  		}
  	}
  	
  	def calculaIndice(valor : Int) : Int = {
  		var mask : Int = 0
  		var masked_n : Int = 0
  		
  		for(i <- 1 to bits_significativos - 1){
  			mask =  1 << bits_significativos - i
  		    masked_n += valor & mask
  		}
  		return masked_n
  	}
  	
  	def divideTabela() {
  		bits_significativos+=1
  		var tam : Int = scala.math.pow(2, bits_significativos).toInt   
  		var novo_bucket : Array[Bucket] = new Array[Bucket](tam)
  		var tamanho_antigo : Int = bucket.length
  		
  		for(i <- 0 to tamanho_antigo-1)
  			novo_bucket(i) = bucket(i)
  		
  		bucket = new Array[Bucket](novo_bucket.length)
  		
  		for(i <- 0 to bucket.length-1){
  			bucket(i) = new Bucket(tamanho_bucket)
  			bucket(i).referencia = bits_significativos
  		}
  		for(i <- 0 to tamanho_antigo-1)
  			for(j <- 0 to tamanho_bucket-1)
  				if(novo_bucket(i).valores(j) != null)
  					insereHash(novo_bucket(i).valores(j))
  		
  	}
  }
}