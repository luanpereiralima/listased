

package ufc.br

object Questao01 {
  
  class Particao{
    var inicio : No = null
    var fim : No = null
    var tamanho = 0
  }
  
  class No{
    var valor : Integer = null
    var representante : No = null
    var proximo : No = null
  }
  
  class Impl{

    def union(par1 : Particao, par2 : Particao) : Particao = {
      
      if(par1==null || par2==null){
        println("Elementos não estão iniciados")
        return null
      }
      
      var maior : Particao = par2
      var menor : Particao = par1
      
      if(par1.tamanho > par2.tamanho){
        maior = par1
        menor = par2
      }
        
      var fim : No = maior.fim
      
      maior.fim = menor.fim
      fim.proximo = menor.inicio
      
      var inicioMenor : No = menor.inicio
      
      while(inicioMenor!=null){
        inicioMenor.representante = maior.inicio
        inicioMenor = inicioMenor.proximo
      }
      
      maior.tamanho+=menor.tamanho
      
      maior
    }
    
    def makeSet(valor : Integer) : Particao = {
      
      var particao : Particao = new Particao()
      var no : No = new No()
      no.valor = valor
      no.representante = no
      no.proximo = null
      
      particao.inicio = no
      particao.fim = no
      particao.tamanho = 1
      
      particao
    }
    
    def find(no : No) : No = {
      no.representante
    }
    
    def print(particao : Particao){
      var no : No = particao.inicio
      while(no!=null){
        println(no.valor)
        no = no.proximo
      }
    }
    
    def liberarEstrutura(particao : Particao) : Particao = {
      particao.inicio = null
      particao.fim = null
      particao.tamanho = 0
      null
    }
   
      
  }
  
   def main(args: Array[String]) {
     var impl : Impl = new Impl
     
     var pa : Particao = impl.makeSet(2)
     
     //impl.print(pa)
     
     var pa2 : Particao = impl.makeSet(5)
     
     //impl.print(pa2)
     
     var parUniao : Particao = impl.union(pa, pa2) 
     
     //impl.print(parUniao)
     
     var pa3 : Particao = impl.makeSet(9)
     
     impl.print(impl.union(parUniao, pa3))
     
     var no1: No = parUniao.inicio
     var no2: No = parUniao.inicio.proximo
     
     if(impl.find(no1).equals(impl.find(no2))){
       println("Os nós contém os mesmos representantes")
     }else{
       println("Nobody")
     }
     
     var estruturaLiberada : Particao = impl.liberarEstrutura(parUniao)
     
     println(estruturaLiberada)
     
    }
  
}