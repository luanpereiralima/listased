
package ufc.br

object Questao03 {
  
  trait Metodos {
    def criarLista() : Lista
    def inserirElemento(lista : Lista, valor : Integer)
    def imprimirValoresDaLista(lista : Lista)
    def imprimirValoresDaListaRecursao(lista : Lista, primeiro : Boolean)
    def imprimirValoresDaListaOrdemReversa(lista : Lista, primeiro : Boolean) : Unit
    def verificarSeListaVazia(lista : Lista): Integer
    def buscarDeterminadoElementoDaLista(lista : Lista, elemento : Integer): Lista
    def removerDeterminadoElementoDaLista(lista: Lista, valor : Integer)
    def removerDeterminadoElementoDaListaRecursao(lista: Lista, valor : Integer, primeiro : Boolean = true)
    def liberarLista(lista : Lista)
    def verificarSeListasSaoIguais(lista1 : Lista, lista2 : Lista) : Boolean
  }
  
  class Lista(valorParam : Integer = null){
    var valor : Integer = valorParam
    var proximo : Lista = null
    var anterior : Lista = null
    override def toString : String = {
      " Valor: "+valor+ "\n"+(if (proximo != null) proximo else "")
    }
  }
  
  class Implementacao extends Metodos{

    override def criarLista() : Lista = {
      new Lista
    }
    
    //OK
    override def inserirElemento(lista : Lista, valor: Integer) : Unit= {      
      var listaTemp = lista
      
      if(listaTemp == null){
        println("A Lista não está iniciada.")
      }else{
        
        if(listaTemp.valor == null){
          listaTemp.valor = valor
          return
        }else{
          do{
            if(listaTemp.proximo == null){
              listaTemp.proximo = new Lista(valor)
              listaTemp.proximo.anterior = listaTemp;
              return
            }
            listaTemp = listaTemp.proximo
          }while(listaTemp!=null)
        }
      }
    }
    
    //OK
    override def imprimirValoresDaLista(lista : Lista) = {
      var listaTemp = lista
      
      if(listaTemp == null || listaTemp.valor == null){
        println("A lista está vazia");
      }else{
        do{
          println(listaTemp.valor)
          listaTemp = listaTemp.proximo
        }while(listaTemp!=null)
      }
    }
    //OK
    override def imprimirValoresDaListaRecursao(lista : Lista, primeiro : Boolean = true) : Unit = {
      var listaTemp = lista
      if(listaTemp == null && primeiro){
        println("A lista está vazia")
        return
      }else if(listaTemp != null){
        println(listaTemp.valor)
        listaTemp = listaTemp.proximo
        imprimirValoresDaListaRecursao(listaTemp, false)
      }
    }
    
    //OK
    override def imprimirValoresDaListaOrdemReversa(lista : Lista, primeiro : Boolean = true) : Unit = {
      if((lista == null || lista.valor == null) && primeiro){
         println("A lista está vazia")
         return
      }else if(lista != null){
          this.imprimirValoresDaListaOrdemReversa(lista.proximo, false)
          println(lista.valor)
      }
    }
    
    //OK
    override def verificarSeListaVazia(lista : Lista): Integer = {
      if (lista == null || lista.valor == null) 1 else 0
    }
    
    //OK
    override def buscarDeterminadoElementoDaLista(lista : Lista, elemento : Integer): Lista = {
      if(lista == null || lista.valor == null){
        println("A lista está vazia")
        null
      }else{
        var listaTemp = lista;
        
        do{
          if(listaTemp.valor == elemento)
            return listaTemp

          listaTemp = listaTemp.proximo;   
        }while(listaTemp != null)
        null
      }
    }
    
    //OK
    override def removerDeterminadoElementoDaLista(lista: Lista, valor : Integer) : Unit = {
        if(lista == null || lista.valor == null){
         println("A lista está vazia")
       }else{       
       
         var tempList = lista
         
         do{
           if(tempList.valor == valor){
           
             if(tempList.anterior!=null){
               tempList.anterior.proximo = tempList.proximo
               
               if(tempList.proximo!=null)
                 tempList.proximo.anterior = tempList.anterior
               
               tempList = null
             }else{
               if(tempList.proximo!=null){
                 tempList.valor = tempList.proximo.valor
                 tempList.proximo = tempList.proximo
               }else{
                 tempList.valor = null;
               }
             }
             return
             
           }else{
             tempList = tempList.proximo
           }
         }while(tempList != null)
       }
    }
    
    //OK
    override def removerDeterminadoElementoDaListaRecursao(lista: Lista, valor : Integer, primeiro : Boolean = true) = {
        if(lista != null){
        var temp = lista
        if(temp.valor == valor){
          
          if(primeiro){
            if(temp.proximo!=null){
              temp.valor = temp.proximo.valor
              temp.proximo = temp.proximo.proximo
              if(temp.proximo!=null)
                temp.anterior = temp
            }else{
              temp.valor = null
            }
          }else{
            if(temp.anterior!=null){
              if(temp.proximo!=null)
                temp.proximo.anterior = temp.anterior
              temp.anterior.proximo = temp.proximo
            }else{
              if(temp.proximo!=null)
                temp.proximo.anterior = temp
              temp = temp.proximo
            }
          }
        }else{
          removerDeterminadoElementoDaListaRecursao(temp.proximo, valor, false)
        }
      }
    }
    
    //OK
    override def liberarLista(lista : Lista) = {
       if(lista == null || lista.valor == null){
         println("A lista está vazia")
       }else{
         var proximo = lista
         var apagar = proximo
         
         proximo.valor = null
         do{
          apagar = proximo
          proximo = proximo.proximo
          apagar = null
         }while(proximo!=null)
       }
    }
    
    override def verificarSeListasSaoIguais(lista1 : Lista, lista2 : Lista) : Boolean = {
      if(lista1 == null || lista1.valor == null){
        println("A lista 1 está vazia.")
        return false
      }
      if(lista2 == null || lista2.valor == null){
        println("A lista 2 está vazia.")
        return false
      }
      
      var lista1Temp = lista1
      var lista2Temp = lista2
      
      do{
        if(lista1Temp.valor != lista2Temp.valor){
          return false
        }
        
        lista1Temp = lista1Temp.proximo
        lista2Temp = lista2Temp.proximo
         
      }while(lista1Temp!=null && lista2Temp!=null)
        
      return true
      
    }
  }
  
    def main (args : Array[String]) : Unit  = {
    
    val impl = new Implementacao
    
    //1 - Criar uma lista vazia;
    val l: Lista = impl.criarLista
    
    //2 - Inserir elemento no início;
    impl.inserirElemento(l, 4)
    impl.inserirElemento(l, 2)
    impl.inserirElemento(l, 1)
    impl.inserirElemento(l, 3)
    
    println("\n3 )")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
    
    println("\n4 )")
    //4 - Imprimir os valores armazenados na lista usando recursão;
    impl.imprimirValoresDaListaRecursao(l)
    
    println("\n5 )")
    //5 - Imprimir os valores armazenados na lista em ordem reversa;
    impl.imprimirValoresDaListaOrdemReversa(l)
    
    println("\n6 )")
    //6 - Verificar se a lista está vazia
    println(impl.verificarSeListaVazia(l))
    
    println("\n7 )")
    //7 - Recuperar/Buscar um determinado elemento da lista;
    println(impl.buscarDeterminadoElementoDaLista(l, 1))
    
     //8. Remover um determinado elemento da lista;
    impl.removerDeterminadoElementoDaLista(l, 1)
        println("\n3 removendo 1)")

    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
     
    impl.removerDeterminadoElementoDaLista(l, 2)
         println("\n3 removendo 2)")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
    
    impl.removerDeterminadoElementoDaLista(l, 0)
          println("\n3 ) removendo 0")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
      
    impl.removerDeterminadoElementoDaLista(l, 3)
           println("\n3 removendo 3)")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
       
    impl.removerDeterminadoElementoDaLista(l, 4)
            println("\n3 removendo 4)")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
    
    impl.removerDeterminadoElementoDaLista(l, 3)
      println("\n3 removendo 3)")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
    
    //9. Remover um determinado elemento da lista usando recursão;
    //impl.removerDeterminadoElementoDaListaRecursao(l, 88)
    
    println("\n10 )")
    //10. Liberar a lista;
    impl.liberarLista(l);
    
     println("\n3 )")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
     
    //11 - Verificando se listas são iguais
    
    //1 - Criar uma lista vazia;
    val l2: Lista = impl.criarLista
    
    //2 - Inserir elemento no início;
    impl.inserirElemento(l2, 4)
    impl.inserirElemento(l2, 2)
    impl.inserirElemento(l2, 1)
    impl.inserirElemento(l2, 3)
    
    impl.inserirElemento(l, 4)
    impl.inserirElemento(l, 2)
    impl.inserirElemento(l, 1)
    impl.inserirElemento(l, 10)
     
    println("Verificando se listas são iguais: "+impl.verificarSeListasSaoIguais(l, l2))
  }
}