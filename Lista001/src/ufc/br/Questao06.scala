
package ufc.br

import java.lang.Double;

object Questao06 {
  
  trait ListaMetodos {
    def criarLista() : Lista
    def inserirElementoNoInicio(lista : Lista, conta : Conta)
    def imprimirValoresDaLista(lista : Lista)
    def imprimirValoresDaListaRecursao(lista : Lista, primeiro : Boolean)
    def imprimirValoresDaListaOrdemReversa(lista : Lista, primeiro : Boolean) : Unit
    def verificarSeListaVazia(lista : Lista): Integer
    def buscarDeterminadoElementoDaLista(lista : Lista, numeroConta : Integer): Lista
    def removerDeterminadoElementoDaLista(lista: Lista, conta : Conta)
    def removerDeterminadoElementoDaListaRecursao(lista: Lista, conta : Conta, primeiro : Boolean = true, anterior : Lista = null)
    def liberarLista(lista : Lista)
  }
  
  trait ContaMetodos {
     def creditar(valor : Double)
     def debitar(valor : Double)
  }
  
  trait ImplementacaoContaMetodos{
    def inserirContaBancaria(tipoDeConta : Integer, valorInicial : Double, numeroConta : Integer) 
    def realizarCreditoEmDeterminadaConta(numeroConta : Integer, valor : Double)
    def realizarDebitoEmDeterminadaConta(numeroConta : Integer, valor : Double)
    def consultarSaldoConta(numeroConta : Integer)
    def consultarBonusContaFidelidade(numeroConta : Integer)
    def realizarTransferênciaEntreDuasContas(numeroConta : Integer, numeroConta2 : Integer, valor : Double)
    def renderJurosContaPoupanca(numeroConta : Integer)
    def renderBonusContaFidelidade(numeroConta : Integer)
    def removerConta(numeroConta : Integer)
    def imprimirNumeroSaldoTodasContasCadastradas()
  }
  
  class Lista(valorParam : Conta = null){
    var conta : Conta = valorParam
    var proximo : Lista = null
  }
  
  class Conta(numeroVal : Integer) extends ContaMetodos{
    private var numero : Integer = numeroVal
    private var saldo : Double = 0
    
    override def creditar(valor : Double){
      this.saldo+=valor
    }
    
    override def debitar(valor : Double){
      this.saldo-=valor
    }
    
    def getNumero() : Integer = {
      this.numero
    }
    
    def getSaldo() : Double = {
      this.saldo
    }
    
    override def toString() : String = {
      "O número da conta é: "+getNumero() + ". \n O valor da conta é : "+getSaldo() + "\n"
    }
  }
  
  class ContaPoupanca(numeroConta : Integer, juros : Double = 0.5) extends Conta (numeroConta){
    def renderJuros(){
      var calculo = (getNumero()*juros) + getNumero()
      this.creditar(calculo)
    }
  }
  
  class ContaFidelidade(numeroConta : Integer, juros : Double = 0.5) extends Conta (numeroConta){
    
    private var bonus : Double = 0
    
    def renderJuros(){
      var calculo = (getNumero()*juros) + getNumero()
      this.creditar(calculo);
    }
    
    override def creditar(valor : Double){
      super.creditar(valor)
      this.bonus+=(valor*0.1)
    }
    
    def renderBonus(){
      super.creditar(this.bonus)
      this.bonus = 0
    }
    
    def getBonus() : Double = {
      this.bonus
    }
  }
  
  class ImplementacaoLista() extends ListaMetodos{

    override def criarLista() : Lista = {
      new Lista
    }
    
    //OK
    override def inserirElementoNoInicio(lista : Lista, conta: Conta) : Unit= {      
      var listaTemp = lista
      
      if(listaTemp == null){
        println("A Lista não está iniciada.")
      }else{
        
        if(listaTemp.conta == null){
          listaTemp.conta = conta
          return
        }else{
          var valorAnterior = listaTemp.conta
          listaTemp.conta = conta
          var novoNo = new Lista(valorAnterior)
          var proximoAnterior = listaTemp.proximo
          listaTemp.proximo = novoNo
          novoNo.proximo = proximoAnterior
        }
      }
    }
    
    //OK
    override def imprimirValoresDaLista(lista : Lista) = {
      var listaTemp : Lista = lista
      
      if(listaTemp == null || listaTemp.conta == null){
        println("A lista está vazia");
      }else{
        do{
          println(listaTemp.conta)
          listaTemp = listaTemp.proximo
        }while(listaTemp!=null)
      }
    }
    
    //OK
    override def imprimirValoresDaListaRecursao(lista : Lista, primeiro : Boolean = true) : Unit = {
      var listaTemp = lista
      if((listaTemp == null  || listaTemp.conta == null) && primeiro){
        println("A lista está vazia")
        return
      }else if(listaTemp != null){
        println(listaTemp.conta)
        listaTemp = listaTemp.proximo
        imprimirValoresDaListaRecursao(listaTemp, false)
      }
    }
    
    //OK
    override def imprimirValoresDaListaOrdemReversa(lista : Lista, primeiro : Boolean = true) : Unit = {
      if((lista == null || lista.conta == null) && primeiro){
         println("A lista está vazia")
         return
      }else if(lista != null){
          this.imprimirValoresDaListaOrdemReversa(lista.proximo, false)
          println(lista.conta)
      }
    }
    
    //OK
    override def verificarSeListaVazia(lista : Lista): Integer = {
      if (lista == null || lista.conta == null) 1 else 0
    }
    
    //OK
    override def buscarDeterminadoElementoDaLista(lista : Lista, numeroConta : Integer): Lista = {
      if(lista == null || lista.conta == null){
        println("A lista está vazia")
        null
      }else{
        var listaTemp = lista;
        
        do{
          if(listaTemp.conta.getNumero() == numeroConta)
            return listaTemp

          listaTemp = listaTemp.proximo;   
        }while(listaTemp != null)
        null
      }
    }
    
    //OK
    override def removerDeterminadoElementoDaLista(lista: Lista, conta : Conta) : Unit = {
       if(lista == null || lista.conta == null){
         println("A lista está vazia")
       }else{       
       
         var tempList = lista
         var anterior : Lista = null
         
         do{
           if(tempList.conta == conta){
           
             if(anterior!=null){
               anterior.proximo = tempList.proximo
               tempList = anterior
             }else{
               if(tempList.proximo!=null){
                 tempList.conta = tempList.proximo.conta
                 tempList.proximo = tempList.proximo.proximo
               }else{
                 tempList.conta = null
               }
             }
             return
             
           }else{
             anterior = tempList
             tempList = tempList.proximo
           }
         }while(tempList != null)
       }
    }
    //OK
    override def removerDeterminadoElementoDaListaRecursao(lista: Lista, conta : Conta, primeiro : Boolean = true, anterior : Lista = null)  = {
      if(lista != null){
        var temp = lista
        if(temp.conta == conta){
          
          if(primeiro){
            if(temp.proximo!=null){
              temp.conta = temp.proximo.conta
              temp.proximo = temp.proximo.proximo
            }else{
              temp.conta = null
            }
          }else{
            if(anterior!=null){
              anterior.proximo = temp.proximo
            }else{
              temp = temp.proximo
            }
          }
        }else{
          removerDeterminadoElementoDaListaRecursao(temp.proximo, conta, false, temp)
        }
      }
    }
    
    //OK
    override def liberarLista(lista : Lista) = {
       if(lista == null || lista.conta == null){
         println("A lista está vazia")
       }else{
         var proximo = lista
         var apagar = proximo
         
         do{
          proximo.conta   = null
          apagar = proximo
          proximo = proximo.proximo
          apagar = null
         }while(proximo!=null)
       }
    }
  }
  
  class ImplementacaoBanco() extends ImplementacaoContaMetodos{
    var implLista = new ImplementacaoLista
    var numeroConta = 1
    var lista : Lista = implLista.criarLista
    
    override def inserirContaBancaria(tipoDeConta : Integer = 1, valorInicial : Double = 0, numeroConta : Integer = this.numeroConta){
      var conta : Conta = null
      
      if(tipoDeConta == 1){
        conta = new Conta(numeroConta)
      }else if(tipoDeConta == 2){
        conta = new ContaPoupanca(numeroConta)
      }else{
        conta = new ContaFidelidade(numeroConta)
      }
      
      conta.creditar(valorInicial)     
      
      implLista.inserirElementoNoInicio(lista, conta)
      this.numeroConta+=1
    }
    override def realizarCreditoEmDeterminadaConta(numeroConta : Integer, valor : Double){
      var listaTemp : Lista = implLista.buscarDeterminadoElementoDaLista(lista, numeroConta)
      
      if(listaTemp == null){
        println("Não existe essa conta.")
      }else{
        listaTemp.conta.creditar(valor)
      }
      
    }
    override def realizarDebitoEmDeterminadaConta(numeroConta : Integer, valor : Double){
      var listaTemp : Lista = implLista.buscarDeterminadoElementoDaLista(lista, numeroConta)
      
      if(listaTemp == null){
        println("Não existe essa conta.")
      }else{
        if(listaTemp.conta.getSaldo() <= valor)
          listaTemp.conta.debitar(valor)
        else
          println("Não existe saldo para essa operação")
      }
    }
    override def consultarSaldoConta(numeroConta : Integer){
      var listaTemp : Lista = implLista.buscarDeterminadoElementoDaLista(lista, numeroConta)
      
      if(listaTemp == null){
        println("Não existe essa conta.")
      }else{
        println("O saldo para essa conta é: "+listaTemp.conta.getSaldo())
      }
    }
    override def consultarBonusContaFidelidade(numeroConta : Integer){
      var listaTemp : Lista = implLista.buscarDeterminadoElementoDaLista(lista, numeroConta)
      
      if(listaTemp == null){
        println("Não existe essa conta.")
      }else{
        var ct : Conta = listaTemp.conta
        if(listaTemp.conta.isInstanceOf[ContaFidelidade]){
         var cf : ContaFidelidade = ct.asInstanceOf[ContaFidelidade]
         println("O bônus dessa conta é: " + cf.getBonus())
        }else{
           println("Essa conta não é do tipo Conta Fidelidade.")
        }
      }
   
    }
    override def realizarTransferênciaEntreDuasContas(numeroConta : Integer, numeroConta2 : Integer, valor : Double){
      var lista1 : Lista = implLista.buscarDeterminadoElementoDaLista(lista, numeroConta)
      var lista2 : Lista = implLista.buscarDeterminadoElementoDaLista(lista, numeroConta2)
      
      if(lista1 == null){
        println("A conta 1 Não existe.")
        return
      }
      
      if(lista2 == null){
        println("A conta 2 Não existe.")
        return
      }
      
      if(!(lista1.conta.getSaldo() <= valor)){
        println("Saldo insificiente na conta 1 para transferência.")
        return
      }
      
      lista1.conta.debitar(valor)
      lista2.conta.creditar(valor)
      
      println("Saldo Transferido.")
    }
    override def renderJurosContaPoupanca(numeroConta : Integer){
      var listaTemp : Lista = implLista.buscarDeterminadoElementoDaLista(lista, numeroConta)
      
      if(listaTemp == null){
        println("Não existe essa conta.")
      }else{
        var ct : Conta = listaTemp.conta
        if(listaTemp.conta.isInstanceOf[ContaFidelidade]){
          var cp : ContaPoupanca = ct.asInstanceOf[ContaPoupanca]
          cp.renderJuros()
          println("Juros rendido.")
        }else{
           println("Essa conta não é do tipo Conta Fidelidade.")
        }
      }
    }
    override def renderBonusContaFidelidade(numeroConta : Integer){
      var listaTemp : Lista = implLista.buscarDeterminadoElementoDaLista(lista, numeroConta)
      
      if(listaTemp == null){
        println("Não existe essa conta.")
      }else{
        var ct : Conta = listaTemp.conta
        if(ct.isInstanceOf[ContaFidelidade]){
          var cf : ContaFidelidade = ct.asInstanceOf[ContaFidelidade]
          cf.renderBonus()
          println("Bônus rendido.")
        }else{
           println("Essa conta não é do tipo Conta Fidelidade.")
        }
      }
    }
    override def removerConta(numeroConta : Integer){
      var listaTemp : Lista = implLista.buscarDeterminadoElementoDaLista(lista, numeroConta)
      
      if(listaTemp == null){
        println("Não existe essa conta.")
      }else{
        implLista.removerDeterminadoElementoDaListaRecursao(lista, listaTemp.conta)
        println("Conta removida")
      }
    }
    override def imprimirNumeroSaldoTodasContasCadastradas(){
      implLista.imprimirValoresDaLista(lista)
    }
    
  }
  
  def main (args : Array[String]) : Unit  = {
      
    var implb : ImplementacaoBanco = new ImplementacaoBanco
    
    var opcao = 0
    
    do{
      
      println("1 - Inserir uma conta bancária")
      println("2 - Inserir uma conta poupança")
      println("3 - Inserir uma conta fidelidade")
      println("4 - Realizar crédito em uma determinada conta")
      println("5 - Realizar débito em uma determinada conta")
      println("6 - Consultar o saldo de uma conta")
      println("7 - Consultar o bônus de uma conta fidelidade")
      println("8 - Realizar uma transferência entre duas contas")
      println("9 - Render juros de uma conta poupança")
      println("10 - Render bônus de uma conta fidelidade")
      println("11 - Remover uma conta")
      println("12 - Imprimir número e saldo de todas as contas cadastradas")
      println("13 - Sair do programa")
      
      opcao = scala.io.StdIn.readInt()
      
      opcao match{
        case 1 => {
          println("Digite o valor inicial da conta: ")
          var valor = scala.io.StdIn.readDouble()
          implb.inserirContaBancaria(1, valor)
        }
        case 2 => {
          println("Digite o valor inicial da conta poupança: ")
          var valor = scala.io.StdIn.readDouble()
          implb.inserirContaBancaria(2, valor)
        }
        case 3 => {
          println("Digite o valor inicial da conta fidelidade: ")
          var valor = scala.io.StdIn.readDouble()
          implb.inserirContaBancaria(3, valor)
        }
        case 4 => {
          println("Digite o número da conta para o crédito: ")
          var numero = scala.io.StdIn.readInt()
          
          println("Digite o valor para o crédito: ")
          var valor = scala.io.StdIn.readDouble()
          
          implb.realizarCreditoEmDeterminadaConta(numero, valor)
        }
        case 5 => {
          println("Digite o número da conta para o crédito: ")
          var numero = scala.io.StdIn.readInt()
          
          println("Digite o valor para o debito: ")
          var valor = scala.io.StdIn.readDouble()
          
          implb.realizarDebitoEmDeterminadaConta(numero, valor)
        }
        case 6 => {
          println("Digite o número da conta para visualizar o bônus: ")
          var numero = scala.io.StdIn.readInt()
          
          implb.consultarBonusContaFidelidade(numero)
        }
        case 7 => {
          println("Digite o número da conta para o crédito: ")
          var numero = scala.io.StdIn.readInt()
          
          implb.consultarBonusContaFidelidade(numero)
        }
        case 8 => {
          println("Digite o número da conta 1 para a transferência para a conta 2: ")
          var numero1 = scala.io.StdIn.readInt()
          
          println("Digite o número da conta 2: ")
          var numero2 = scala.io.StdIn.readInt()
          
          println("Digite o valor da transferência: ")
          var valor = scala.io.StdIn.readDouble()
          
          implb.realizarTransferênciaEntreDuasContas(numero1, numero2, valor)
        }
        case 9 => {
          println("Digite o número da conta poupança para render juros: ")
          var numero = scala.io.StdIn.readInt()
          
          implb.renderJurosContaPoupanca(numero)
        }
        case 10 => {
          println("Digite o número da conta fidelidade para render bônus: ")
          var numero = scala.io.StdIn.readInt()
          
          implb.renderBonusContaFidelidade(numero)
        }
        case 11 => {
          println("Digite o número da conta para a remoção: ")
          var numero = scala.io.StdIn.readInt()
          
          implb.removerConta(numero)
        }
        case 12 => {
          implb.imprimirNumeroSaldoTodasContasCadastradas()
        }
        case 13 => println("Até mais!")
        
        case _ => println("Digite uma opção válida")
      }
    }while(opcao!=13)
  }
}